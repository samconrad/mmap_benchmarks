/*
 * =====================================================================================
 *
 *       Filename:  mmap_bench.cc
 *
 *    Description:  Micro benchmark for mmap and munmap calls
 *
 *        Version:      1.0
 *        Created:      07/23/2016 07:13:49 PM
 *       Revision:      none
 *       Compile with:  gcc -Wall -lrt -O3 mmap_bench.cc -o mmap_bench
 *
 *         Author:  Sam Conrad
 *   Organization:  
 *
 * =====================================================================================
 */

#include <assert.h>
#include <errno.h>
#include <sched.h>
#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>

static void     benchmark(const size_t, const double);
static void     open_tmp_files(const size_t, const size_t, int*);
static void     close_tmp_files(const size_t, int*);
static void     set_affinity();
static uint64_t read_rdtsc_start();
static uint64_t read_rdtsc_end();
static double   calibrate_cycles_per_ns();

static const size_t g_num_iters = 1000;

#define get_nanos(tps) tps.tv_sec * 1000000000 + tps.tv_nsec
#define exit_with_errno() printf("errno msg: %s\n", strerror(errno)); exit(1);

//uncomment for debugging
//#define ENABLE_ASSERT

/* 
 * Run benchmarks with different file sizes.
 * Lazy cleanup of tmp directory with rm.
 */
int main() {
    //calibration
    set_affinity();
    printf("Warning: if cpu scaling is enabled, results may be inaccurate\n");
    const double cycles_per_ns = calibrate_cycles_per_ns();
    printf("Cycles per ns: %.03f\n", cycles_per_ns);

    size_t sizes[] = {1024, 1024 * 1024, 16 * 1024 * 1024};
    size_t i;
    for (i = 0; i < 3; i++) {
        printf("Running benchmark for file size: %ld\n", sizes[i]);
        benchmark(sizes[i], cycles_per_ns);
        system("rm /tmp/mmap_bench*");
    }

    return 0;
}

static void benchmark(const size_t file_size, const double cycles_per_ns)
{
    int fds[g_num_iters];
    memset(fds, 0, sizeof(int) * g_num_iters);
    open_tmp_files(g_num_iters, file_size, fds);

    void* ptrs[g_num_iters];
    int *fd_ptr = fds;
    void **ptr = ptrs;

    //bench mmap
    const uint64_t start_mmap = read_rdtsc_start();
    for (size_t i = 0; i < g_num_iters; i++) 
    {
        *ptr++ = mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, *fd_ptr++, 0);
#ifdef ENABLE_ASSERT
        assert(MAP_FAILED != *(ptr - 1));
#endif
    }
    const uint64_t end_mmap = read_rdtsc_end();

    fd_ptr = fds;
    ptr = ptrs;

    //write some random data so sync has dirty pages to write
    for (size_t i = 0; i < g_num_iters; i++) 
    {
        memset(*ptr++, 1, file_size);
    }

    fd_ptr = fds;
    ptr = ptrs;

    //bench msync
    const uint64_t start_msync = read_rdtsc_start();
    for (size_t i = 0; i < g_num_iters; i++) 
    {
#ifdef ENABLE_ASSERT
        assert(-1 != msync(*ptr++, file_size, MS_SYNC));
#else
        msync(*ptr++, file_size, MS_SYNC);
#endif
    }
    const uint64_t end_msync = read_rdtsc_end();

    fd_ptr = fds;
    ptr = ptrs;

    //bench munmap
    const uint64_t start_munmap = read_rdtsc_start();
    for (size_t i = 0; i < g_num_iters; i++) 
    {
#ifdef ENABLE_ASSERT
        assert(-1 != munmap(*ptr++, file_size));
#else
        munmap(*ptr++, file_size);
#endif
    }
    const uint64_t end_munmap = read_rdtsc_end();

    close_tmp_files(g_num_iters, fds);

    const uint64_t mmap_nanos = (end_mmap - start_mmap) / (g_num_iters / cycles_per_ns);
    const uint64_t msync_nanos = (end_msync - start_msync) / (g_num_iters / cycles_per_ns);
    const uint64_t munmap_nanos = (end_munmap - start_munmap) / (g_num_iters / cycles_per_ns);

    printf("mmap nanos: %lu\n", mmap_nanos);
    printf("msync nanos: %lu\n", msync_nanos);
    printf("munmap nanos: %lu\n", munmap_nanos);
}

static void set_affinity() 
{
    cpu_set_t mask;
    CPU_ZERO(&mask);
    //set core affinity to 1
    CPU_SET(1, &mask);
    assert(0 == sched_setaffinity(0, sizeof(mask), &mask));
}

static void open_tmp_files(const size_t num_files, const size_t file_size, int* out_fds)
{
    const char* const tmplt = "/tmp/mmap_benchXXXXXX"; 
    char buf[1024];

    for (size_t i = 0; i < num_files; i++)
    {
        strncpy(buf, tmplt, 1024);
        const int fd = mkstemp(buf);

        if (-1 == fd)
        {
            exit_with_errno();
        }

        *(out_fds + i) = fd;

        if(-1 == ftruncate(fd, file_size))
        {
            exit_with_errno();
        }
    }

}

static void close_tmp_files(const size_t num_files, int* in_fds)
{
    for (size_t i = 0; i < num_files; i++)
    {
        if (-1 == close(*(in_fds++)))
        {
            exit_with_errno();
        }
    }
}

/*
 * Note: strategy for RDTSC reads is based on Gabriele Paoloni's white paper
 * (http://www.intel.com/content/www/us/en/embedded/training/ia-32-ia-64-benchmark-code-execution-paper.html)
 */

static inline uint64_t read_rdtsc_start()
{
    uint32_t hi = 0, lo = 0;
    asm volatile (
            "mfence\n\t"
            "rdtsc\n\t"
            : "=a" (lo), "=d" (hi)
            :: "memory");
    const uint64_t result = lo | ((uint64_t)hi << 32);
    return result;
}

static inline uint64_t read_rdtsc_end()
{
    uint32_t hi = 0, lo = 0;
    //rdtscp additionally clobbers cx, when it reads cpu id
    asm volatile (
            "rdtscp\n\t"
            "mfence\n\t"
            : "=a" (lo), "=d" (hi)
            :: "%rcx", "memory");
    const uint64_t result = lo | ((uint64_t)hi << 32);
    return result;
}

static double calibrate_cycles_per_ns() {
    //sleep for 4 seconds, just needs to be long enough so the overhead of clock_gettime and rdtsc is made trivial
    const useconds_t sleep_time = 4 * 1000 * 1000; 
    struct timespec tps_start, tps_end;

    clock_gettime(CLOCK_REALTIME, &tps_start);
    const uint64_t cycles_start = read_rdtsc_start();

    usleep(sleep_time);

    const uint64_t cycles_end = read_rdtsc_end();
    clock_gettime(CLOCK_REALTIME, &tps_end);

    const uint64_t total_cycles = cycles_end - cycles_start;
    const uint64_t total_nanos = get_nanos(tps_end) - get_nanos(tps_start);
    const double cycles_per_nano = total_cycles / (double) total_nanos;

    assert(cycles_per_nano > 0.0);
    return cycles_per_nano;
}


